﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using webapp.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webapp.Controllers
{
    public class InternshipController : Controller
    {
        static readonly HttpClient client = new HttpClient();

        // GET: /<controller>/
        public IActionResult Index()
        {
            var r = getList();
            
            return View("../List/Index", r.Result);
        }

        private async Task<IEnumerable<Internship>> getList()
        {

            HttpResponseMessage result = await client.GetAsync("https://localhost:3001/api/internship");
            var jsonString = result.Content.ReadAsStringAsync();

            IEnumerable<Internship> model = JsonConvert.DeserializeObject<IEnumerable<Internship>>(jsonString.Result);

            return model;
        }

    }
}
