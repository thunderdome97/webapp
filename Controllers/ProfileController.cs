﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using webapp.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace webapp.Controllers
{
    public class ProfileController : Controller
    {
        static readonly HttpClient client = new HttpClient();
        // GET: /Profile

        [HttpGet]
        public ActionResult Index()
        {
            int uId =(int) HttpContext.Session.GetInt32("UserId");


            var r = getUser(uId);

            ViewBag.Data = "ID: "+uId;

            //TempData["UserMessage"] = new AppMessage() { CssClassName = "alert-danger  alert-dismissible", Title = "Error", Message = "Onjuiste combinatie van email en wachtwoord." };
            return View(r.Result);

        }

        [HttpPost]
        public async Task<IActionResult> Edit(User u)
        {
           

            var r = await updateUserdata(u);

            if (r.IsSuccessStatusCode)
                return View("Index", u);

            TempData["UserMessage"] = new AppMessage() { CssClassName = "alert-danger  alert-dismissible", Title = "Error:", Message = " Fout tijdens opslaan gegevens! Probeer het opnieuw" };
            return View("Index");

        }
        private async Task<User> getUser(int id)
        {
           
            HttpResponseMessage result = await client.GetAsync("https://localhost:3001/api/user/"+ id);
            var jsonString = result.Content.ReadAsStringAsync();

            User model = JsonConvert.DeserializeObject<User>(jsonString.Result);

            return model;
        }

        private async Task<HttpResponseMessage> updateUserdata(User u)
        {

            string loginJson = JsonConvert.SerializeObject(new { u.FirstName, u.MiddleName, u.LastName, u.Email, u.Password });
            StringContent content = new StringContent(loginJson, Encoding.UTF8, "application/json");
            HttpResponseMessage result = await client.PutAsync("https://localhost:3001/api/user/" + HttpContext.Session.GetInt32("UserId"), content);
            var jsonString = result.Content.ReadAsStringAsync();

            //User model = JsonConvert.DeserializeObject<User>(jsonString.Result);


            return result;
        }
    }
}
