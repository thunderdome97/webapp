﻿using System;
namespace webapp.Models
{
    public class AppMessage
    {
        public string CssClassName { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }

        public AppMessage()
        {
        }
    }
}
