﻿using System;
using System.Runtime.Serialization;

namespace webapp.Models
{
    [DataContract]
    public class Student : User
    {
        [DataMember]
        public int StudentId { get; set; }
        [DataMember]
        public int ClassId { get; set; }
        [DataMember]
        public int Crebo { get; set; }

        public Student()
        {
        }
    }
}
