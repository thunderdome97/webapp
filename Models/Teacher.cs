﻿using System;
using System.Runtime.Serialization;

namespace webapp.Models
{
    [DataContract]
    public class Teacher : User
    {
        [DataMember]
        public string TeacherCode { get; set; }
        [DataMember]
        public int Location { get; set; }
        [DataMember]
        public bool Mentor { get; set; }
        [DataMember]
        public int ClassId { get; set; }

        public Teacher()
        {
        }
    }
}
